﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dto
{
    public class Dto_users
    {
        private string login;
        private string pass;
        private string name;      
        private string phone;
        private string cmnd;

        public string Login
        {
            get { return login; }
            set { login = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string Cmnd
        {
            get { return cmnd; }
            set { cmnd = value; }
        }
        public string Pass
        {
            get { return pass; }
            set { pass = value; }
        }
        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }

    }
}
