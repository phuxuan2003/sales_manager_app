﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dto;
using Dal;

namespace Bus
{
    public class Bus_update_info
    {
        Dal_update_info dal = new Dal_update_info();

        public void Get_info(Dto_users data, string Login)
        {
            dal.get_info(data, Login);
        }

        public void Update_Info(Dto_users data, string Login)
        {
            dal.update_information(data, Login);
        }
    }
}
