﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dto;
using Dal;
namespace Bus
{
    public class  Bus_forget_password
    {
        Dal_forget_password dal = new Dal_forget_password();
        public bool Check_per_id_phone(Dto_users data)
        {
            return dal.check_code_pid_phone(data);
        }
        public void Update_pass(Dto_users data)
        {
            dal.update_password(data);
        }
    }
}
