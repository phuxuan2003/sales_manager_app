﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Dal;
namespace Bus
{
    public class Bus_menu
    {
        Dal_menu dal = new Dal_menu();
        public DataTable Customer()
        {
            return dal.customer();
        }

        public DataTable Ward()
        {
            return dal.ward();
        }

        public DataTable District()
        {
            return dal.district();
        }

        public DataTable City()
        {
            return dal.city();
        }

        public DataTable Country()
        {
            return dal.country();
        }

        public DataTable Template()
        {
            return dal.template();
        }

        public DataTable Product()
        {
            return dal.product();
        }

        public DataTable Payment()
        {
            return dal.payment();
        }
        public DataTable Orders()
        {
            return dal.orders();
        }

        public DataTable Location()
        {
            return dal.location();
        }

        public DataTable Stock_quantity()
        {
            return dal.stock_quantity();
        }

        public DataTable Orders(string date)
        {
            return dal.orders(date);
        }
        public string Amount_total(string date)
        {
            return dal.amount_total(date);
        }

    }
}
