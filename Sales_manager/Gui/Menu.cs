﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bus;

namespace Gui
{
    public partial class Menu : Form
    {
        public string Login ;
        public Menu(string Login)
        {
            this.Login = Login;
            InitializeComponent();
            user_name();
        }
        public void user_name()
        {
            this.label1.Text = "User name: " + Login;
        }
        Bus_menu bus = new Bus_menu();
        private void ribbonButton1_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = bus.Customer();
            dataGridView1.Columns[0].Width = 50;
            dataGridView1.Columns[0].HeaderText = "ID";

            dataGridView1.Columns[1].Width = 110;
            dataGridView1.Columns[1].HeaderText = "Name";

            dataGridView1.Columns[2].Width = 50;
            dataGridView1.Columns[2].HeaderText = "Code";

            dataGridView1.Columns[3].Width = 50;
            dataGridView1.Columns[3].HeaderText = "Loyalty";

            dataGridView1.Columns[4].Width = 60;
            dataGridView1.Columns[4].HeaderText = "Street";

            dataGridView1.Columns[5].Width = 60;
            dataGridView1.Columns[5].HeaderText = "Ward";

            dataGridView1.Columns[6].Width = 60;
            dataGridView1.Columns[6].HeaderText = "District";

            dataGridView1.Columns[7].Width = 70;
            dataGridView1.Columns[7].HeaderText = "City";

            dataGridView1.Columns[8].HeaderText = "Country";

            dataGridView1.Columns[9].HeaderText = "Amount total";

            dataGridView1.Columns[10].HeaderText = "Phone number";

            dataGridView1.Columns[11].HeaderText = "Tax";
        }

        private void ribbonButton2_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = bus.Ward();

            dataGridView1.Columns[0].Width = 220;
            dataGridView1.Columns[0].HeaderText = "ID";

            dataGridView1.Columns[1].Width = 220;
            dataGridView1.Columns[1].HeaderText = "Name";

            dataGridView1.Columns[2].Width = 220;
            dataGridView1.Columns[2].HeaderText = "Code";

            dataGridView1.Columns[3].Width = 230;
            dataGridView1.Columns[3].HeaderText = "District ID";
        }

        private void ribbonButton3_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = bus.District();

            dataGridView1.Columns[0].Width = 230;
            dataGridView1.Columns[0].HeaderText = "ID";

            dataGridView1.Columns[1].Width = 230;
            dataGridView1.Columns[1].HeaderText = "Name";

            dataGridView1.Columns[2].Width = 220;
            dataGridView1.Columns[2].HeaderText = "Code";

            dataGridView1.Columns[3].Width = 230;
            dataGridView1.Columns[3].HeaderText = "City ID";
        }

        private void ribbonButton4_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = bus.City();

            dataGridView1.Columns[0].Width = 220;
            dataGridView1.Columns[0].HeaderText = "ID";

            dataGridView1.Columns[1].Width = 220;
            dataGridView1.Columns[1].HeaderText = "Name";

            dataGridView1.Columns[2].Width = 230;
            dataGridView1.Columns[2].HeaderText = "Code";

            dataGridView1.Columns[3].Width = 240;
            dataGridView1.Columns[3].HeaderText = "Country ID";
        }

        private void ribbonButton5_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = bus.Country();

            dataGridView1.Columns[0].Width = 310;
            dataGridView1.Columns[0].HeaderText = "ID";

            dataGridView1.Columns[1].Width = 300;
            dataGridView1.Columns[1].HeaderText = "Name";

            dataGridView1.Columns[2].Width = 300;
            dataGridView1.Columns[2].HeaderText = "Code";
        }

        private void ribbonButton6_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = bus.Template();

            dataGridView1.Columns[0].Width = 150;
            dataGridView1.Columns[0].HeaderText = "ID";

            dataGridView1.Columns[1].Width = 150;
            dataGridView1.Columns[1].HeaderText = "Name";

            dataGridView1.Columns[2].Width = 150;
            dataGridView1.Columns[2].HeaderText = "Warranty Month";

            dataGridView1.Columns[3].Width = 150;
            dataGridView1.Columns[3].HeaderText = "Height";

            dataGridView1.Columns[4].Width = 160;
            dataGridView1.Columns[4].HeaderText = "Weight";

            dataGridView1.Columns[5].Width = 150;
            dataGridView1.Columns[5].HeaderText = "Country ID";
        }

        private void ribbonButton7_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = bus.Product();

            dataGridView1.Columns[0].Width = 150;
            dataGridView1.Columns[0].HeaderText = "ID";

            dataGridView1.Columns[1].Width = 150;
            dataGridView1.Columns[1].HeaderText = "Name";

            dataGridView1.Columns[2].Width = 150;
            dataGridView1.Columns[2].HeaderText = "Code";

            dataGridView1.Columns[3].Width = 160;
            dataGridView1.Columns[3].HeaderText = "Template ID";

            dataGridView1.Columns[4].Width = 150;
            dataGridView1.Columns[4].HeaderText = "Price";

            dataGridView1.Columns[5].Width = 150;
            dataGridView1.Columns[5].HeaderText = "Loyalty";
        }

        private void ribbonButton8_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = bus.Payment();

            dataGridView1.Columns[0].Width = 300;
            dataGridView1.Columns[0].HeaderText = "ID";

            dataGridView1.Columns[1].Width = 310;
            dataGridView1.Columns[1].HeaderText = "Name";

            dataGridView1.Columns[2].Width = 300;
            dataGridView1.Columns[2].HeaderText = "Code";
        }

        private void ribbonButton9_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = bus.Orders();

            dataGridView1.Columns[0].Width = 140;
            dataGridView1.Columns[0].HeaderText = "ID";

            dataGridView1.Columns[1].Width = 130;
            dataGridView1.Columns[1].HeaderText = "Code";

            dataGridView1.Columns[2].Width = 130;
            dataGridView1.Columns[2].HeaderText = "Date";

            dataGridView1.Columns[3].Width = 130;
            dataGridView1.Columns[3].HeaderText = "Customer ID";

            dataGridView1.Columns[4].Width = 130;
            dataGridView1.Columns[4].HeaderText = "Amount total";

            dataGridView1.Columns[5].Width = 150;
            dataGridView1.Columns[5].HeaderText = "Loyalty total";
        }

        private void ribbonButton10_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = bus.Product();

            dataGridView1.Columns[0].Width = 150;
            dataGridView1.Columns[0].HeaderText = "ID";

            dataGridView1.Columns[1].Width = 160;
            dataGridView1.Columns[1].HeaderText = "Name";

            dataGridView1.Columns[2].Width = 150;
            dataGridView1.Columns[2].HeaderText = "Code";

            dataGridView1.Columns[3].Width = 150;
            dataGridView1.Columns[3].HeaderText = "Template ID";

            dataGridView1.Columns[4].Width = 150;
            dataGridView1.Columns[4].HeaderText = "Price";

            dataGridView1.Columns[5].Width = 150;
            dataGridView1.Columns[5].HeaderText = "Loyalty";
        }

        private void ribbonButton11_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = bus.Location();


            dataGridView1.Columns[0].Width = 310;
            dataGridView1.Columns[0].HeaderText = "ID";

            dataGridView1.Columns[1].Width = 300;
            dataGridView1.Columns[1].HeaderText = "Name";

            dataGridView1.Columns[2].Width = 300;
            dataGridView1.Columns[2].HeaderText = "Code";
        }

        private void ribbonButton12_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = bus.Stock_quantity();

            dataGridView1.Columns[0].Width = 220;
            dataGridView1.Columns[0].HeaderText = "ID";

            dataGridView1.Columns[1].Width = 230;
            dataGridView1.Columns[1].HeaderText = "Product ID";

            dataGridView1.Columns[2].Width = 220;
            dataGridView1.Columns[2].HeaderText = "Location ID";

            dataGridView1.Columns[3].Width = 240;
            dataGridView1.Columns[3].HeaderText = "Quantity";

        }

        private void ribbonButton13_Click(object sender, EventArgs e)
        {
            DateTime check_date;
            if (DateTime.TryParse(ribbonTextBox1.TextBoxText, out check_date))
            {
                dataGridView1.DataSource = bus.Orders(ribbonTextBox1.TextBoxText);

                dataGridView1.Columns[0].Width = 140;
                dataGridView1.Columns[0].HeaderText = "ID";

                dataGridView1.Columns[1].Width = 130;
                dataGridView1.Columns[1].HeaderText = "Code";

                dataGridView1.Columns[2].Width = 130;
                dataGridView1.Columns[2].HeaderText = "Date";

                dataGridView1.Columns[3].Width = 130;
                dataGridView1.Columns[3].HeaderText = "Customer ID";

                dataGridView1.Columns[4].Width = 130;
                dataGridView1.Columns[4].HeaderText = "Amount total";

                dataGridView1.Columns[5].Width = 150;
                dataGridView1.Columns[5].HeaderText = "Loyalty total";

                ribbonPanel15.Text = "Amount total in " + ribbonTextBox1.TextBoxText;
                ribbonLabel1.Text = bus.Amount_total(ribbonTextBox1.TextBoxText);
            }      
        }

        private void ribbonButton14_Click(object sender, EventArgs e)
        {
            Update_info udi = new Update_info(this.Login);
            udi.Show();
        }
    }
}
