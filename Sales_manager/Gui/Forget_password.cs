﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bus;
using Dto;

namespace Gui
{
    public partial class Forget_password : Form
    {
        public Forget_password()
        {
            InitializeComponent();
        }
        Bus_forget_password bus = new Bus_forget_password();

        private void button1_Click(object sender, EventArgs e)
        {
            Dto_users data = new Dto_users();
            if (button1.Text == "Check")
            {
                data.Login = textBox1.Text;
                data.Phone = textBox3.Text;
                data.Cmnd = textBox5.Text;
                if (bus.Check_per_id_phone(data))
                {
                    button1.Text = "Reset";
                    textBox1.Visible = false;
                    textBox3.Visible = false;
                    textBox5.Visible = false;
                    textBox2.Visible = true;
                    textBox4.Visible = true;
                }
                else
                {
                    MessageBox.Show("Wrong information!");
                }

            }
            else
            {
                if (textBox2.Text == textBox4.Text)
                {
                    data.Login = textBox1.Text;
                    data.Pass = textBox2.Text;
                    bus.Update_pass(data);
                    MessageBox.Show("Reset password successfully!");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Check your password again!");
                }
            }
        }
    }
}
