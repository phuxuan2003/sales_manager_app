﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dto;
using Bus;

namespace Gui
{
    public partial class Update_info : Form
    {
        public string Login;
        public Update_info(string Login)
        {
            this.Login = Login;
            InitializeComponent();
            get_info();
        }
        Bus_update_info bus = new Bus_update_info();
        public void get_info()
        {
            Dto_users data = new Dto_users();
            bus.Get_info(data, Login);
            textBox1.Text = data.Name;
            textBox2.Text = data.Phone;
            textBox3.Text = data.Cmnd;
            textBox4.Text = data.Login;
            textBox5.Text = data.Pass;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if ((textBox1.Text != "") && (textBox2.Text != "") && (textBox3.Text != "") && (textBox4.Text != "") && (textBox5.Text != ""))
            {
                Dto_users data = new Dto_users();
                data.Name = textBox1.Text;
                data.Phone = textBox2.Text;
                data.Cmnd = textBox3.Text;
                data.Login = textBox4.Text;
                data.Pass = textBox5.Text;
                bus.Update_Info(data, Login);
                MessageBox.Show("Update successfully!");
                this.Close();
            } else
            {
                MessageBox.Show("Your information is not valid!\nPlease check and up date again!");
            }
        }
    }
}
