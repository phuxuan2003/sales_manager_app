﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bus;
using Dto;

namespace Gui
{
    public partial class Sign_up : Form
    {
        public Sign_up()
        {
            InitializeComponent();
        }
        Bus_sign_up bus = new Bus_sign_up();
        private void button1_Click(object sender, EventArgs e)
        {
            try 
            { 
                Dto_users data = new Dto_users();
                data.Name = textBox1.Text;
                data.Phone = textBox2.Text;
                data.Cmnd = textBox3.Text;
                data.Login = textBox4.Text;
                data.Pass = textBox5.Text;
                bus.Sign_up(data);
                MessageBox.Show("Sign up successfully!");
                this.Close();
            } 
            catch
            {
                MessageBox.Show("Your information is not valid!\nPlease check and sign up again!");
            }
        }
    }
}
