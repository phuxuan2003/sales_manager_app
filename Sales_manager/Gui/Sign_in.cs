﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bus;
using Dto;

namespace Gui
{
    public partial class Sign_in : Form
    {
        string Login;
        public Sign_in()
        {
            InitializeComponent();
        }

        Bus_sign_in bus = new Bus_sign_in();
        private void button1_Click(object sender, EventArgs e)
        {
            if (button1.Text == "hide")
            {
                button1.Text = "show";
            }
            else
            {
                button1.Text = "hide";
            }
            textBox2.UseSystemPasswordChar = !textBox2.UseSystemPasswordChar;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "")
            {
                Dto_users data = new Dto_users();
                data.Login = textBox1.Text;
                data.Pass = textBox2.Text;
                if (bus.Check_account(data))
                {
                    Login = textBox1.Text;
                    DialogResult res = MessageBox.Show("Sign in successfully!\nDo you want to update your information?", "", MessageBoxButtons.YesNo);
                    if (res == DialogResult.Yes)
                    {
                        Menu mn = new Menu(Login);
                        mn.Show();
                        Update_info udi = new Update_info(Login);
                        udi.Show();
                    }
                    else
                    {
                        Menu mn = new Menu(Login);
                        mn.Show();
                    }                  
                }
                else
                {
                    MessageBox.Show("Wrong account!");
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Forget_password fg = new Forget_password();
            fg.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Sign_up su = new Sign_up();
            su.Show();
        }
    }
}
