﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace Dal
{
    public class Dal_menu : DB_connect
    {
        // xuat ra bang customer
        public DataTable customer()
        {
            con.Open();
            SqlCommand cm = new SqlCommand(@"Select * from customer", con);
            SqlDataAdapter da = new SqlDataAdapter(cm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        // xuat ra bang ward
        public DataTable ward()
        {
            con.Open();
            SqlCommand cm = new SqlCommand(@"Select * from ward", con);
            SqlDataAdapter da = new SqlDataAdapter(cm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        // xuat ra bang district
        public DataTable district()
        {
            con.Open();
            SqlCommand cm = new SqlCommand(@"Select * from district", con);
            SqlDataAdapter da = new SqlDataAdapter(cm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        // xuat ra bang city
        public DataTable city()
        {
            con.Open();
            SqlCommand cm = new SqlCommand(@"Select * from city", con);
            SqlDataAdapter da = new SqlDataAdapter(cm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        // xuat ra bang country
        public DataTable country()
        {
            con.Open();
            SqlCommand cm = new SqlCommand(@"Select * from country", con);
            SqlDataAdapter da = new SqlDataAdapter(cm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        // xuat ra bang template
        public DataTable template()
        {
            con.Open();
            SqlCommand cm = new SqlCommand(@"Select * from template", con);
            SqlDataAdapter da = new SqlDataAdapter(cm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        // xuat ra bang product
        public DataTable product()
        {
            con.Open();
            SqlCommand cm = new SqlCommand(@"Select * from product", con);
            SqlDataAdapter da = new SqlDataAdapter(cm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        // xuat ra bang detail_orders
        public DataTable payment()
        {
            con.Open();
            SqlCommand cm = new SqlCommand(@"Select *  from payment", con);
            SqlDataAdapter da = new SqlDataAdapter(cm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        // xuat ra bang orders
        public DataTable orders()
        {
            con.Open();
            SqlCommand cm = new SqlCommand(@"Select * from orders", con);
            SqlDataAdapter da = new SqlDataAdapter(cm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }


        // xuat ra bang locations
        public DataTable location()
        {
            con.Open();
            SqlCommand cm = new SqlCommand(@"Select id,name,code from locations", con);
            SqlDataAdapter da = new SqlDataAdapter(cm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        // xuat ra bang stock_quantity
        public DataTable stock_quantity()
        {
            con.Open();
            SqlCommand cm = new SqlCommand(@"Select * from stock_quantity", con);
            SqlDataAdapter da = new SqlDataAdapter(cm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        // xuat ra bang orders trong ngay
        public DataTable orders( string date)
        {
            con.Open();
            SqlCommand cm = new SqlCommand(@"Select * from orders where date ='"+date+"'", con);
            SqlDataAdapter da = new SqlDataAdapter(cm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        //xuat ra amount total trong ngay
        public string amount_total(string date)
        {
            con.Open();
            SqlCommand cm = new SqlCommand(@"Select sum(amount_total) as Sum from orders where date ='"+date+"'", con);
            SqlDataReader sdr = cm.ExecuteReader();
            sdr.Read();
            string sum = sdr["Sum"].ToString();
            con.Close();
            return sum;
        }
    }
}
